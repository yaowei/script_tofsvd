#!/usr/bin/env bash

source /programs/biogrids.shrc
mkdir ../StringTie

project_dir=TOFSVD
script_dir=script_tofsvd

for bam in /n/scratch3/users/y/yl477/${project_dir}/bam/*.bam; do

sbatch -p short -t 0-2:00 -c 4 --mem 16G --job-name StringTie-H --wrap="sh /n/scratch3/users/y/yl477/${project_dir}/${script_dir}/rna-seq-StringTie_final.sh $bam"
sleep 1	# wait 1 second between each job submission
done