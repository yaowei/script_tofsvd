#!/bin/bash

#SBATCH -c 8
#SBATCH -N 1

#SBATCH -p short
#SBATCH --job-name bamtobw
#SBATCH --mem 16gb
#SBATCH --time 0-12:00

source /programs/biogrids.shrc
export PATH=/home/yl477/software/kentutils:$PATH

/home/yl477/software/bamTobw/bamTobw.sh -b $1 -s -l 150