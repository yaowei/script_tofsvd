#!/usr/bin/env bash

# This script takes a fastq file of RNA-Seq data, runs FastQC and outputs a counts file for it.
# USAGE: sh rnaseq_analysis_on_allfiles.sh <name of fastq file>

# initialize a variable with an intuitive name to store the name of the input fastq file
module load hisat2/2.1.0
module load gcc/6.2.0
module load samtools/1.10
fq1=$1
fq2=$2

#hisat2index=/home/yl477/Genome_files/HISAT2_Index/grcm38_snp_tran/genome_snp_tran
hisat2index=/home/yl477/Genome_files/HISAT2_Index/grch38_snp_tran/genome_snp_tran
#galias=grcm38
galias=grch38
P=4

# grab base of filename for naming outputs
base=$(basename ${fq1} |cut -f 1 -d ".");
echo "Sample name is $base"           

# Run HISAT2 and samtools sequentially,output sorted .bam file

hisat2 --dta -p ${P} -x ${hisat2index} -1 ${fq1} -2 ${fq2} -S ${base}_${galias}.sam
echo "Sorting ${f}..."
samtools view -bS -@ ${P} ${base}_${galias}.sam | samtools sort -@ ${P} -o ${base}_${galias}_sorted.bam && echo ${f}" bam sorted" && samtools index ${base}_${galias}_sorted.bam
rm ${base}_${galias}.sam
mv ${base}_${galias}_sorted.bam ${base}_${galias}_sorted.bam.bai ../bam
echo "DONE"
